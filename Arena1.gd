extends Node2D

export (PackedScene) var Viking;
export var death_score = 25;
var score = 0;
var highscore = 0;

func _ready():
	$Player.hide();
	$Player/CollisionShape2D.set_deferred("disabled", true);
	randomize();
	$Player.position = $PlayerSpawn.position;
	$HUD/Score.text = str(highscore);


func _on_MobTimer_timeout():
	$MobPath/MobSpawnLocation.offset = randi();
	var viking = Viking.instance();
	add_child(viking);
	$Player.connect("hit_enemy", viking, "_on_Player_hit_enemy");
	viking.connect("hit", $Player, "_on_Viking_hit");
	viking.connect("hit", self, "game_over");
	viking.connect("death_score", self, "_on_Viking_death_score");
	viking.position = $MobPath/MobSpawnLocation.position;


func new_game():
	$HUD/RetryButton.hide();
	$HUD/QuitButton.hide();
	$Player.show();
	$Player/CollisionShape2D.disabled = false;
	$ScoreTimer.start();
	$MobTimer.start();
	$Player.can_move = true;
	$Player.set_process_input(true);
	$Player.position = $PlayerSpawn.position;
	score = 0;
	$HUD/Score.text = str(score);
	


func quit_game():
	get_tree().quit();


func game_over():
	$MobTimer.stop();
	$ScoreTimer.stop();
	# show game over hud
	yield(get_tree().create_timer(3), "timeout");
	get_tree().call_group("vikings", "queue_free");
	$HUD/RetryButton.show();
	$HUD/QuitButton.show();
	highscore = max(highscore, score);
	$HUD/Trash.text = "Highscore: " + str(highscore); 


func _on_ScoreTimer_timeout():
	print(score)
	score += 1;
	$HUD/Score.text = str(score);


func _on_Viking_death_score():
	score += death_score;
	$HUD/Score.text = str(score);


func _on_HUD_ready():
	$HUD/RetryButton.connect("pressed", self, "new_game");
	$HUD/QuitButton.connect("pressed", self, "quit_game");
