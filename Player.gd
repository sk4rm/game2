extends KinematicBody2D

signal hit_enemy(damage, target_viking);

export var speed = 400;
export var damage = 500;
var can_move = true;
var queue_combo = false;
var motion = Vector2();
var screen_size;
# hitbox index: attack 1: left/right = hitbox1/hitbox3
#				attack 2: left/right = hitbox2/hitbox4


func _ready():
	screen_size = get_viewport_rect().size;


func _input(_event):
	# attack
	if Input.is_action_just_pressed("ui_attack"):
		can_move = false
		motion = Vector2();
		
		# if facing left, offset -8 otherwise offset 8 (x axis)
		if $AnimatedSprite.flip_h:
			$AnimatedSprite.offset.x = -8;
		else:
			$AnimatedSprite.offset.x = 8;
		
		if $AnimatedSprite.animation == "attack3" \
		or $AnimatedSprite.animation == "attack2" \
		or $AnimatedSprite.animation == "attack1":
			queue_combo = true;
		elif $AnimatedSprite.animation != "attack1":
			$AnimatedSprite.animation = "attack1";


func _process(_delta):
	if !$AnimatedSprite.flip_h:
		if ($AnimatedSprite.animation == "attack1" and $AnimatedSprite.frame == 5) \
		or ($AnimatedSprite.animation == "attack2" and $AnimatedSprite.frame == 1):
			$Hitbox1/CollisionShape2D.disabled = false;
		elif $AnimatedSprite.animation == "attack3" and $AnimatedSprite.frame == 3:
			$Hitbox2/CollisionShape2D.disabled = false;
		else:
			$Hitbox1/CollisionShape2D.set_deferred("disabled", true);
			$Hitbox2/CollisionShape2D.set_deferred("disabled", true);
	else:
		if ($AnimatedSprite.animation == "attack1" and $AnimatedSprite.frame == 5) \
		or ($AnimatedSprite.animation == "attack2" and $AnimatedSprite.frame == 1):
			$Hitbox3/CollisionShape2D.disabled = false;
		elif $AnimatedSprite.animation == "attack3" and $AnimatedSprite.frame == 3:
			$Hitbox4/CollisionShape2D.disabled = false;
		else:
			$Hitbox3/CollisionShape2D.set_deferred("disabled", true);
			$Hitbox4/CollisionShape2D.set_deferred("disabled", true);


func _physics_process(_delta):
	if can_move:
		# horizontal movement scripts
		if Input.is_action_pressed("ui_right"):
			motion.x = speed;
			$AnimatedSprite.flip_h = motion.x < 0;
		elif Input.is_action_pressed("ui_left"):
			motion.x = -speed;
			scale.x = 1;
			$AnimatedSprite.flip_h = motion.x < 0;
		else:
			motion.x = 0;
		
		# vertical movement scripts
		if Input.is_action_pressed("ui_down"):
			motion.y = speed;
		elif Input.is_action_pressed("ui_up"):
			motion.y = -speed;
		else:
			motion.y = 0;
		
		# animation bits
		if motion.length() != 0:
			$AnimatedSprite.animation = "run";
		else:
			$AnimatedSprite.animation = "idle";
	
	# reduce diagonal speed
	motion = motion.normalized() * speed;
	motion = move_and_slide(motion, Vector2(0, -1));
	# boundaries
	position.x = clamp(position.x, 0, screen_size.x);
	position.y = clamp(position.y, 0, screen_size.y);


func _on_AnimatedSprite_animation_finished():
	# animations for combo (one tap, double tap, triple tap)
	if $AnimatedSprite.animation == "attack1":
		if queue_combo:
			$AnimatedSprite.animation = "attack2";
			queue_combo = false;
		else:
			$AnimatedSprite.animation = "attackend";
	elif $AnimatedSprite.animation == "attack2":
		if queue_combo:
			$AnimatedSprite.animation = "attack3";
			queue_combo = false;
		else:
			$AnimatedSprite.animation = "attackend";
	elif $AnimatedSprite.animation == "attack3":
		$AnimatedSprite.animation = "attackend";
		queue_combo = false;
		if $AnimatedSprite.flip_h:
			position.x -= 78;
		else:
			position.x += 78;
		$AnimatedSprite.animation = "attackend";
	elif $AnimatedSprite.animation == "attackend":
		$AnimatedSprite.animation = "idle";
		$AnimatedSprite.offset.x = 0;
		can_move = true;
	elif $AnimatedSprite.animation == "death":
		self.hide();
		if !$CollisionShape2D.disabled:
			$CollisionShape2D.set_deferred("disabled", true);


func _on_Hitbox_body_shape_entered(_body_id, body, _body_shape, _area_shape):
	if "Viking" in body.name:
		emit_signal("hit_enemy", damage, body);


func _on_Viking_hit():
	# player response when he gets hit
	$CollisionShape2D.set_deferred("disabled", true);
	can_move = false;
	set_process_input(false);
	$AnimatedSprite.animation = "death";
	pass;
