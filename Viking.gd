extends KinematicBody2D

signal hit;
signal death_score;

export var speed = 2;
export var health = 1000;
var can_move = true;
var target = null;

func _process(_delta):
	if $AnimatedSprite.animation == "attack" and $AnimatedSprite.frame == 2:
		if $AnimatedSprite.flip_h:
			$AttackLeft/CollisionPolygon2D.disabled = false;
		else:
			$AttackRight/CollisionPolygon2D.disabled = false;
	else:
		if $AnimatedSprite.flip_h:
			$AttackLeft/CollisionPolygon2D.set_deferred("disabled", true);
		else:
			$AttackRight/CollisionPolygon2D.set_deferred("disabled", true);

func _physics_process(_delta):
	var velocity = Vector2();
	if can_move:
		# execute if target has a body
		if target:
			velocity = global_position.direction_to(target.global_position);
			velocity *= speed;
			$AnimatedSprite.flip_h = velocity.x < 0;
			move_and_collide(velocity);
		
		if velocity.length() != 0:
			$AnimatedSprite.animation = "run";
		else:
			$AnimatedSprite.animation = "idle";


# target player
func _on_LineOfSight_body_shape_entered(_body_id, body, _body_shape, _area_shape):
	if body.name == "Player":
		target = body;


# don't care about anything else
func _on_LineOfSight_body_shape_exited(_body_id, body, _body_shape, _area_shape):
	if body != null:
		if body.name == "Player":
			target = null;
			$AnimatedSprite.animation = "idle";


func _on_AttackRange_body_shape_entered(_body_id, body, _body_shape, _area_shape):
	if body != null:
		if body.name == "Player":
			# attack
			can_move = false;
			$AnimatedSprite.animation = "idle";
			$AttackTimer.start();


func _on_AttackRange_body_shape_exited(_body_id, body, _body_shape, _area_shape):
	if body != null:
		if body.name == "Player":
			# stop attacking if not already attacking
			$StandbyTimer.start();
			$AnimatedSprite.animation = "idle";
			$AttackTimer.stop();


func _on_AttackTimer_timeout():
	$AnimatedSprite.animation = "attack";


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "attack":
		$AnimatedSprite.animation = "idle";


# stall for a bit before continue chasing after in atk range
func _on_StandbyTimer_timeout():
	can_move = true;


func _on_Player_hit_enemy(damage, target_viking):
	target_viking.health -= damage;
	
	# flash red when hit
	target_viking.get_node("AnimatedSprite").modulate = Color8(255, 0, 0);
	yield(get_tree().create_timer(0.1), "timeout");
	target_viking.get_node("AnimatedSprite").modulate = Color8(255, 255, 255);
	
	# die
	if health <= 0:
		emit_signal("death_score");
		queue_free();


func _on_Hitbox_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.name == "Player":
		emit_signal("hit");
